<?php


if (!defined("_ECRIRE_INC_VERSION")) return;


// Declaration des tables
function benevolat_declarer_tables_objets_sql($tables){

$tables['spip_benevolat_taches'] = array(
				'page'=>'benevolat_taches',
				'texte_retour' => 'benevolat:icone_retour_tache',
				'texte_modifier' => 'benevolat:icone_modifier_tache',
				'texte_creer' => 'benevolat:icone_ecrire_tache',
				'texte_objets' => 'benevolat:tache_benevoles',
				'texte_objet' => 'benevolat:tache_benevole',
				'texte_signale_edition' => 'benevolat:texte_travail_benevole',
				'info_aucun_objet'=> 'benevolat:info_aucune_tache_benevole',
				'info_1_objet' => 'benevolat:info_1_tache_benevole',
				'info_nb_objets' => 'benevolat:info_nb_tache_benevoles',
				'texte_logo_objet' => 'benevolat:logo_tache_benevole',
				'texte_langue_objet' => 'benevolat:titre_langue_tache_benevole',
				'titre' => 'nom AS titre',
				'date' => 'date_creation',
				'principale' => 'oui',
				'champs_editables' => array('nom','details','duree','competence','nb_personne','regularite','echeance','fini','public','commentaire'),
				'field' => array(
						  "id_benevolat_tache"	=> "bigint(21) NOT NULL auto_increment",
						  "nom" 				=> "text NOT NULL default ''",
						  "details" 			=> "text NOT NULL default ''",
						  "duree" 				=> "text NOT NULL default ''",
						  "competence" 			=> "text NOT NULL default ''",
						  "nb_personne"			=> "text NOT NULL default ''",
						  "regularite"			=> "text NOT NULL default ''",
						  "echeance"			=> "text NOT NULL default ''",
						  "commentaire"			=> "text NOT NULL default ''",
						  "date_creation" 		=> "datetime NULL",
						  "fini" 				=> "TINYINT(1) default 0",
						  "public" 				=> "TINYINT(1) default 0",
						  "maj" 				=> "timestamp NOT NULL"),
				'key' => array(
						"PRIMARY KEY" 		=> "id_benevolat_tache",
						"KEY fini" 			=> "fini",
						"KEY date_creation"	=> "date_creation",
				),
				'rechercher_champs' => array(),
				'rechercher_jointures' => array(
					'nom' => array('nom' => 10),
					'details' => array('nom' => 3),
				),
				'tables_objet'=>"benevolat_taches",
				'type'=>"benevolat_tache",
			);

$tables['spip_benevolat_participants'] = array(
				'texte_retour' => 'benevolat_participant:icone_retour_participant',
				'texte_modifier' => 'benevolat_participant:icone_modifier_participant',
				'texte_creer' => 'benevolat_participant:icone_ecrire_participant',
				'texte_objets' => 'benevolat_participant:participants',
				'texte_objet' => 'benevolat_participant:participant',
				'texte_signale_edition' => 'benevolat_participant:texte_travail_benevole',
				'info_aucun_objet'=> 'benevolat_participant:info_aucun_paticipant',
				'info_1_objet' => 'benevolat_participant:info_1_tache_benevole',
				'info_nb_objets' => 'benevolat_participant:info_nb_participants',
				'texte_logo_objet' => 'benevolat_participant:logo_participant',
				'texte_langue_objet' => 'benevolat_participant:titre_langue_tache_benevole',
				'titre' => 'nom AS titre',
				'date' => 'date_inscription',
				'principale' => 'non',
				'champs_editables' => array('id_benevolat_tache','id_auteur','nom','courriel','telephone'),
				'field' => array(
						  "id_benevolat_participant"	=> "bigint(21) NOT NULL auto_increment",
						  "id_benevolat_tache"	=> "bigint(21) NOT NULL",
						  "id_auteur" 			=> "bigint(21) NULL",
						  "nom" 				=> "text NOT NULL default ''",
						  "courriel" 			=> "text NOT NULL default ''",
						  "telephone" 			=> "text NOT NULL default ''",
						  "date_inscription" 	=> "date NULL",
						  "maj" 				=> "timestamp NOT NULL"),
				'key' => array(
						"PRIMARY KEY" 			=> "id_benevolat_participant",
						"KEY id_benevolat_tache"=> "id_benevolat_tache",
						"KEY date_inscription"	=> "date_inscription",
				),
				'rechercher_champs' => array(),
				'rechercher_jointures' => array(
					'nom' => array('nom' => 10)
				),
				'tables_objet'=>"benevolat_participants",
				'type'=>"benevolat_participant",
			);



	return $tables;
}



function benevolat_declarer_tables_auxiliaires($tables)
{


    return $tables;
}


function benevolat_declarer_tables_interfaces($tables)
{
	$tables['table_des_tables']['benevolat_taches'] = 'benevolat_taches';
	$tables['table_des_tables']['benevolat_participants'] = 'benevolat_participants';
	$tables['table_des_traitements']['DETAILS']['benevolat_taches'] = _TRAITEMENT_RACCOURCIS;
	
	return  $tables;
}




//Pipeline. Compatibilite avec le plugin ChampsExtra2
function benevolat_objets_extensibles($objets){
        return array_merge($objets, array(	'benevolat_taches' 	=> _T('asso:info_benevolat_libelle_taches')));
}





function benevolat_declarer_champs_extras($champs = array()){

return $champs;

}



function benevolat_rechercher_liste_des_champs($tables){


$tables['asso_membre']['prenom'] = 5;
$tables['asso_membre']['identifiant_interne'] = 1;
$tables['asso_membre']['commentaire'] = 1;
return $tables;
}



function benevolat_rechercher_liste_des_jointures($tables){
$tables['auteur']['asso_membre']['prenom'] = 3;
$tables['auteur']['asso_membre']['commentaire'] = 1;
$tables['asso_membre']['auteur']['nom'] = 3;
$tables['asso_membre']['auteur']['email'] = 5;
$tables['asso_membre']['auteur']['bio'] = 1;
return $tables;
}



