# Pré-requis

Avoir un serveur LAMP avec un spip installé en version 3.2.
Pour le téléchargement et la gestion des dépendances (librairie js), il vous 
faut avoir installer yarn sur votre systeme.

# Installation en local

## Cloner le repository SPIP

```
git clone git@framagit.org:robin-des-bio/rdb-squelette-spip.git
cd squelettes
yarn
```

A mettre de préférence dans /var/www/html (il faudra changer les doits (```sudo chmod g+w robindesbio/ -R```) pour accéder au dossier)

## Installer les plugins SPIP

Puis voila un petit bash pour installer tous les plugins spip qui vont bien
```
#!/bin/bash
paquets=(ancres_douces blocsdepliables comments-300 compositions_v3 crayons modeles_facebook_s3 inserer_modeles_v1 magnet menus_1 minibando-dev noie nospam numerotation 'pages' 'pays_v3' 'saisies_v2' 'socialtags' 'sociaux' 'spip-bonux-3' 'verifier' 'yaml' 'z-core')
for i in ${paquets[@]}; do
	wget http://files.spip.org/spip-zone/${i}.zip
	unzip -o ${i}.zip -d plugins/auto
	rm ${i}.zip
done
exit 0
```
ce script est à executer à la racine du site.

Note: des plugins supplémentaires devront probablement être installés, voir avec Guillaume si besoin

## Installer autres plugins  

* MariaDB
* libapache2-mod-php7
* éditer un truc dans /etc/php/7.0/apache2/php.ini (voir avec Guillaume)

Certainement d'autres trucs qui font faire que ça marche toujours pas -> voir avec Guillaume :D

## Installer la base de données

Une copie de la base de donnée est disponible sur le nuage (nuage.robindesbio.org) dans le dossier "Sauvegarde_site".
Le dossier est à dézipper et à mettre à la racine du site.


## Installation de SPIP

Se rendre sur l'adresse local http:127.0.0.1/robindesbio/ecrire pour terminer l'installation de spip.


## Les plugins à activer à minima

Aller voir dans la liste en production

# Utilisation des librairies et outils (gulp et ses modules)

Lors du développement, quand vous souhaitez modifier les feuilles de styles ou 
les js, n'oubliez pas de lancer gulp dans une console à part,
il s'occupera de compiler les fichiers à chaque modifications des 
fichiers sources.

Pour savoir ce que fait exactement gulp, il suffit de lire
dans le fichier `squelettes/Gulpfile.js`


# Editer le site Internet

Quelques infos de base:

* Les pages du site sont dans squelette
* La page d'accueil est dans squelettes/content/sommaire.html






