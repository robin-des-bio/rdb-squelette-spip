<?php
function inserer_rollover($texte,$rollover) {
      $onmouseover=" onmouseover=\"this.src='".extraire_attribut($rollover, "src")."'\"";
      $onmouseout=" onmouseout=\"this.src='".extraire_attribut($texte, "src")."'\"";
      $texte = (preg_replace("/(<img.*?)(\/>)/i", "\$1$onmouseover$onmouseout \$2", $texte));
      return $texte;
}

function tri_docart($tab,$key_tri='date',$sens=''){
$tab_tri=array();
foreach ($tab as $key => $row) {
    $tab_tri[$key]  = strtolower(wd_remove_accents($row[$key_tri]));
}
array_multisort($tab_tri, SORT_STRING, $tab);
if($sens=='')$tab=array_reverse($tab);
return $tab;
}

function wd_remove_accents($str, $charset='utf-8')
{
    $str = htmlentities($str, ENT_NOQUOTES, $charset);
    
    $str = preg_replace('#&([A-za-z])(?:acute|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    
    return $str;
}


function navbar_responsive($nav, $class_collapse = 'nav-collapse-main'){
    if (strpos($nav,'nav-collapse')!==false) return $nav;

    $respnav = '';

    $uls = extraire_balises($nav,"ul");
    $n = 1;
    while ($ul = array_shift($uls)
        AND strpos(extraire_attribut($ul,"class"),"nav")===false){
        $n++;
    }
    if ($ul){
        $respnav = $nav;
        $p = strpos($respnav,$ul);
        $respnav = substr_replace($respnav,
            ' <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>' .
            "\n".'<div class="nav-collapse ' . $class_collapse . ' collapse">',$p,0);
        $l=strlen($respnav);$p=$l-1;
        while ($n--){
            $p = strrpos($respnav,"</ul>",$p-$l);
        }
        if ($p)
            $respnav = substr_replace($respnav,'</div>',$p+5,0);
        else
            $respnav = $nav;
    }
    return $respnav;
}

function filtre_lien_ou_expose_dist($url,$libelle=NULL,$on=false,$class="",$title="",$rel="", $evt=''){
    if ($on) {
        $bal = 'strong';
        $class = "";
        $att = "";
        // si $on passe la balise et optionnelement une ou ++classe
        // a.active span.selected.active etc....
        if (is_string($on) AND (strncmp($on,'a',1)==0 OR strncmp($on,'span',4)==0 OR strncmp($on,'strong',6)==0)){
            $on = explode(".",$on);
            // on verifie que c'est exactement une des 3 balises a, span ou strong
            if (in_array(reset($on),array('a','span','strong'))){
                $bal = array_shift($on);
                $class = implode(" ",$on);
                if ($bal=="a")
                    $att = 'href="#" ';
            }
        }
        $att .= 'class="'.($class?attribut_html($class).' ':'').'on active"';
    } else {
        $bal = 'a';
        $att = "href='$url'"
            .($title?" title='".attribut_html($title)."'":'')
            .($class?" class='".attribut_html($class)."'":'')
            .($rel?" rel='".attribut_html($rel)."'":'')
            .$evt;
    }
    if ($libelle === NULL)
        $libelle = $url;
    return "<$bal $att>$libelle</$bal>";
}

