const {watch, series, parallel,src,dest} = require('gulp');

const jshint = require('gulp-jshint');
const gulpsass = require('gulp-sass')(require('sass'));
const fiber = require('fibers')
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');
const googleWebFonts = require('gulp-google-webfonts');
const faMinify = require('gulp-fa-minify');

const config = {
    nodeDir: './node_modules/',
    bootstrapDir: './node_modules/bootstrap/',
    dir: './',
    publicDir: '../'
};

const fichiers = {
    'bootstrap': [
        config.dir + 'scss/bootstrap/scss/bootstrap.scss',
    ],
    'bootstrap2spip': [
        config.dir + 'scss/bootstrap2spip/css/spip.reboot.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.list.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.table.scss',
        config.dir + 'scss/bootstrap2spip/css/_utilities.scss',
        config.dir + 'scss/bootstrap2spip/css/spip.css'
    ],
    'sass': [
        config.dir + 'fonts/fonts.css',
        config.dir + 'css/bootstrap.css',
        config.dir + 'css/bootstrap2spip.css',
        config.nodeDir + 'sidr/dist/stylesheets/jquery.sidr.light.css',
        //config.dir + 'scss/animation.scss',
        config.dir + 'scss/perso.scss',
        config.dir + 'scss/explorateur.scss',
        config.dir + 'scss/sommaire.scss'
    ],
    'scripts': [
        //config.nodeDir + 'isotope-layout/dist/isotope.pkgd.js',
        //config.nodeDir +'select2/dist/js/select2.js',
        //config.nodeDir +'select2/dist/js/select2.full.js',
        //config.nodeDir +'select2/dist/js/i18n/fr.js',
        //config.nodeDir + 'daterangepicker/moment.min.js',
        //config.nodeDir + 'daterangepicker/daterangepicker.js',
        //config.nodeDir + 'bootstrap-datepicker/js/bootstrap-datepicker.js',
        //config.nodeDir + 'bootstrap-datepicker/js/locales/bootstrap-datepicker.fr.js',
        //config.nodeDir + 'bootstrap-timepicker/js/bootstrap-timepicker.js',
        //config.nodeDir +'velocity-animate/velocity.js',
        //config.nodeDir +'velocity-animate/velocity.ui.js',
        config.nodeDir + 'sidr/dist/jquery.sidr.js',
        config.dir + 'js/*.js'
    ],
    'scripts_bas_de_page': [
        config.bootstrapDir + 'dist/js/bootstrap.bundle.js',

    ]
};


const icones_fa = {
    fal: [],
    far: [],
    fas: ['at','rss','redo','exclamation-triangle','calendar','newspaper','bars','print','sign-out-alt','user'],
    fab: ['mastodon','facebook']
};




exports.bootstrap = bootstrap;
exports.sass = sass;
exports.css = series( bootstrap,bootstrap2spip,sass);

exports.scripts = scripts;
exports.scripts_bas_de_page = scripts_bas_de_page;
exports.verif_js = series(lint);
exports.js = series(scripts,scripts_bas_de_page);

exports.flag =  parallel( flag_sass,flag_copie);
exports.copie_fonts = copie_fonts;
exports.fonts = series(googlefonts,copie_fonts,sass);
exports.fa = series( fontawesome,scripts);
exports.typo = parallel( exports.fonts,exports.fa);

exports.template = series( templates,exports.js)

exports.build = parallel(series(scripts,scripts_bas_de_page),exports.css);

exports.watch = function() {
    watch(config.dir + 'js/**/*.js',scripts);
    watch(config.dir + 'scss/**/*.scss', sass);
    watch(config.dir + 'html/**/*.html', templates);
};

exports.default = series(exports.build,exports.watch);









/////////////////////////////////////////////////////
// fonctions FEUILLES DE STYLES
////////////////////////////////////////////////////

function bootstrap(cb) {

    src(fichiers.bootstrap)
        .pipe(gulpsass(
                {
                    includePaths: [config.dir + 'scss/bootstrap/scss/', config.bootstrapDir + 'scss/'],
                }
            ).on('error', gulpsass.logError)
        )
        .pipe(cleanCSS({compatibility: 'ie8', rebase: false}))
        .pipe(concat('bootstrap.css'))
        .pipe(dest(config.dir + 'css'));
    cb();
}


function bootstrap2spip(cb) {

    src(fichiers.bootstrap2spip)
        .pipe(gulpsass(
                {
                    includePaths: [config.dir + 'scss/bootstrap2spip/', config.dir + 'scss/bootstrap', config.bootstrapDir],
                }
            ).on('error', gulpsass.logError)
        )
        .pipe(cleanCSS({compatibility: 'ie8', rebase: false}))
        .pipe(concat('bootstrap2spip.css'))
        .pipe(dest(config.dir + 'css'));
    cb();
}


function sass(cb) {
    src(fichiers.sass)
        .pipe(gulpsass(
                {
                    includePaths: [config.dir + 'scss/bootstrap', config.bootstrapDir + 'scss/'],
                    fiber: fiber
                }
            ).on('error', gulpsass.logError)
        )
        .pipe(cleanCSS({compatibility: 'ie8', rebase: false}))
        .pipe(concat('perso.css'))
        .pipe(dest(config.publicDir + 'css'));
    cb();
}


/////////////////////////////////////////////////////
// fonctions SCRIPTS JS
////////////////////////////////////////////////////


// Lint Task
function lint(cb) {
    src(config.dir + 'js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
    cb();
}

function scripts(cb) {
    src(fichiers.scripts)
        .pipe(concat('scripts.js'))
        .pipe(uglify())
        .pipe(dest(config.publicDir + 'js'));
    cb();
}

function scripts_bas_de_page(cb) {
    src(fichiers.scripts_bas_de_page)
        .pipe(concat('scripts_footer.js'))
        .pipe(uglify())
        .pipe(dest(config.publicDir + 'js'));
    cb();
}



/////////////////////////////////////////////////////
// fonctions icones et typo
////////////////////////////////////////////////////

function googlefonts(cb) {
    src(config.dir+'fonts.list')
        .pipe(googleWebFonts({
            fontsDir:'../fonts'
        }))
        .pipe(dest(config.dir+'fonts/'));
    cb();
}

function copie_fonts(cb) {
    src([
        config.dir+'fonts/*.{ttf,woff,woff2,eof,svg}'
    ])
        .pipe(dest(config.publicDir+'fonts'));
    cb();
}


function fontawesome(cb) {
    src(config.nodeDir + '@fortawesome/fontawesome-free/js/all.js')
        .pipe(rename('all.fa-min.js'))
        .pipe(faMinify(icones_fa))
        .pipe(uglify())
        .pipe(dest(config.dir + 'js'));
    cb();
}



function flag_sass(cb) {
    src([
        config.dir+'sass/flag-icon.sass'
    ]).pipe(gulpsass({
        includePaths: [config.dir + 'scss', config.nodeDir+'flag-icon-css/sass']
    }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('flag.css'))
        .pipe(dest(config.publicDir+'css'));
    cb();
}


function flag_copie(cb) {
    src([
        config.nodeDir+'flag-icon-css/flags/**/*.svg'
    ]).pipe(dest(config.publicDir+'flags'));
    cb();
}

/////////////////////////////////////////////////////
// fonctions template
////////////////////////////////////////////////////

function templates(cb) {
    src(config.dir + 'templates/**/*.html')
        .pipe(template({namespace: 'templates'}))
        .pipe(concat('templates.js'))
        .pipe(dest(config.dir + 'js/'));
    cb();
}

