<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_desinscrire_benevolat_participant_dist(){
	$securiser_action = charger_fonction('securiser_action','inc');
	$arg = $securiser_action();
	$id_benevolat_participant = intval($arg);
	sql_delete('spip_benevolat_participants','id_benevolat_participant = '.$id_benevolat_participant);

	return;
}
?>
