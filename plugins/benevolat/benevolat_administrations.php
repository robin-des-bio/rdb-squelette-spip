<?php
/***************************************************************************\
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Fran�ois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/meta');
include_spip('base/create');
include_spip('inc/cextras');

function benevolat_upgrade($nom_meta_base_version, $version_cible){

	$maj = array();
	
	$maj['create'] = array(
			array('maj_tables', array('spip_benevolat_taches','spip_benevolat_participants')),
	);
	include_spip('base/upgrade');
	include_spip('base/benevolat');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);

}


function benevolat_vider_tables($nom_meta_base_version) {

	$tables_a_supprimer=array('spip_benevolat_taches','spip_benevolat_participants');
	foreach($tables_a_supprimer as $table)
		{
		sql_drop_table($table);
		spip_log("$table $nom_meta_base_version desinstalle");
		}
	effacer_meta($nom_meta_base_version);
	spip_log("$table $nom_meta_base_version desinstalle");

}


function benevolat_peupler_base()
{

}


