<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_participer_benevolat_tache_dist($arg=null){
	
	
	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	
	$id_benevolat_tache = intval($arg);
	$participant=sql_allfetsel('*','spip_benevolat_participants','id_benevolat_tache='.$id_benevolat_tache);
	
	
	$tab_participant=array();
	if(!empty($participant))
		$tab_participant=explode(";",$participant);
	include_spip('inc/session');
	$id_auteur=session_get('id_auteur');
	if(!in_array($id_auteur,$tab_participant)){
		$tab_participant[]=trim($id_auteur);

		}
	return ;
}
?>
