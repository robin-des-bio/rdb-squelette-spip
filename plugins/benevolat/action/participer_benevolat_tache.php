<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_participer_benevolat_tache_dist(){
	$securiser_action = charger_fonction('securiser_action','inc');
	$arg = $securiser_action();
	$id_benevolat_tache = intval($arg);
	include_spip('inc/session');
	$id_auteur=session_get('id_auteur');
	$deja_participant=sql_getfetsel('id_auteur','spip_benevolat_participants','id_benevolat_tache='.$id_benevolat_tache.' and id_auteur='.$id_auteur);
	if(!$deja_participant){
		sql_insertq('spip_benevolat_participants',array('id_benevolat_tache'=>$id_benevolat_tache,'id_auteur'=>$id_auteur,'date_inscription'=>date('Y-m-d H:i:s')));
		}

	include_spip('inc/config');
	include_spip('inc/notifications');
	$emails=lire_config('benevolat/notifications');
	notifications_envoyer_mails($emails, 'Un adhérent vient de s\'inscrire sur le tableau des bénévoles', "[RdB] Inscription bénévolat");
	
		
	return ;
}
?>
