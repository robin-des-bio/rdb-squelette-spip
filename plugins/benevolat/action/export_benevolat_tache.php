<?php


if (!defined('_ECRIRE_INC_VERSION'))
	return;

function utf8_decode_array($array) { // Encode toutes les entrées d'un tableau en utf8 sur 3 niveaux.
    // Premier niveau
    foreach($array as $c=>$v) {
        // Second niveau
        if(is_array($array[$c])) {
            foreach($array[$c] as $c2=>$v2) {
                // troisieme niveau
                if(is_array($array[$c][$c2]))
                    $array[$c][$c2] = array_map("utf8_decode", $array[$c][$c2]);
                else
                    $array[$c][$c2] = utf8_decode($array[$c][$c2]);
            }
        }
        else
            $array[$c] = utf8_decode($array[$c]);
    }
    return($array);
}


function action_export_benevolat_tache()
{
	
	$tab_tache=sql_allfetsel('*','spip_benevolat_taches');
	foreach($tab_tache as &$tache){
			$tache['participant']=sql_getfetsel('count(*)','spip_benevolat_participants','id_benevolat_tache='.$tache['id_benevolat_tache']);
			if($tache['participant']>0){$tache['participant'].=' participant';if($tache['participant']>1){$tache['participant'].='s';}}else $tache['participant']='';
	}
		include_spip("lib/tbs/tbs_class");
		include_spip("lib/open_tbs/tbs_plugin_opentbs");
		$TBS = new clsTinyButStrong;
		$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
		$template=dirname(dirname(__FILE__))."/export_benevolat_tache.ods";

		$x = pathinfo($template);
		$template_ext = $x['extension'];
		if (!file_exists($template)) exit("File does not exist.");
		
		
		
		
		// Load the template
		$TBS->LoadTemplate($template);

		if ($debug==2) { // debug mode 2
			$TBS->Plugin(OPENTBS_DEBUG_XML_CURRENT);
			exit;
		} elseif ($debug==1) { // debug mode 1
			$TBS->Plugin(OPENTBS_DEBUG_INFO);
			exit;
		}

		// Merge data
	//	var_dump($tab_tache);
		$TBS->MergeBlock('a', utf8_decode_array($tab_tache));
		
		
		
		
		
		//echo(realpath(_DIR_TMP)."/test.ods".'<br>');
		$file=realpath(_DIR_TMP)."/test.ods";
		$TBS->Show(OPENTBS_DOWNLOAD, 'liste_des_taches_benevoles_'.date('Y-m-d').'.ods');
		//$TBS->saveOds($object,$file); //save the object to a ods file
		//header("Content-Type: application/vnd.ms-excel");
		//header("Content-disposition: attachment; filename=\"table_declaration".time().".ods\"");
		//header('Pragma: public');
		//header('Content-Length: '.filesize($file));
		//echo(readfile($file));
	
    
}



?>
