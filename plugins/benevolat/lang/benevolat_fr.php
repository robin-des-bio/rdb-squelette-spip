<?php
/***************************************************************************
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'benevolat' => 'Bénévolat',
	'courriel'=>'Courriel',

	'icone_retour_tache'=>'Retour',
	'icone_modifier_tache'=>'Modifier la tâche',
	'icone_ecrire_tache'=>'Ajouter une nouvelle tâche',
	'info_aucune_tache_benevole'=>'Aucune tâche',
	'info_1_tache_benevole'=>'tâche bénévole',
	'info_nb_tache_benevoles'=>'tâches bénévole',
	
	'je_veux_participer'=>'Je veux participer',
	
	'liste_des_taches'=>'Liste des tâches',
	
	'nom_et_prenom'=>'Nom et prénom',
	
	's_inscrire' => 'S\'inscrire',
	
	'titre_page_config' => 'Configuration du plugin',
	'tache_benevoles'=>'Tâches bénévoles',
	'tache_benevole'=>'tâche bénévole',
	'telephone'=>'Téléphone'

);







?>
