<?php
if (!defined("_ECRIRE_INC_VERSION")) return;


// fonction pour le pipeline, n'a rien a effectuer
function benevolat_autoriser(){}
// declarations d'autorisations
function autoriser_benevolat_bouton_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('modifier', 'benevolat', $id, $qui, $opt);
}
function autoriser_benevolat_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('modifier', 'benevolat', $id, $qui, $opt);
}

function autoriser_benevolat_modifier_dist($faire, $type, $id, $qui, $opt) {
	include_spip('inc/config');
	include_spip('inc/session');

	$tab_user=lire_config('benevolat/gestionnaires');
    return (in_array($qui['statut'], ['0minirezo']) || in_array(session_get('id_auteur'), $tab_user));
}

function autoriser_benevolattache_voir_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('voir', 'benevolat', $id, $qui, $opt);
}
function autoriser_benevolattache_modifier_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('modifier', 'benevolat', $id, $qui, $opt);
}

function autoriser_benevolat_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('voir', 'benevolat', $id, $qui, $opt);
}

