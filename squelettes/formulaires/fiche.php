<?php

function formulaires_fiche_charger_dist($id_rubrique,$redirect=''){	
	$valeurs = array();
	
	$valeurs['id_rubrique'] = $id_rubrique;
	$valeurs['intitule_fiche'] = '';
	$valeurs['domaine_fiche'] = '';
	$valeurs['sous_domaine_fiche'] = array();
	$valeurs['descriptif_fiche'] = '';
	$valeurs['texte_fiche'] = '';
	$valeurs['organisme_fiche'] = '';
	$valeurs['telephone_fiche'] = '';
	$valeurs['autorisation']=true;
	$valeurs['modification'] = false;

	if(!empty($id_article))
		{
			
		if(!$res=sql_fetsel('*','spip_auteurs_articles','id_article='.($id_article+0).' and id_auteur='.session_get('id_auteur')))
		{
			return array('autorisation'=>false);
		}
	}

		if($res=sql_fetsel('titre','spip_rubriques','id_rubrique='.($valeurs['id_rubrique']+0)))
			{
				$valeurs['domaine_fiche'] = $res['titre'];
			}
			
		if($res=sql_fetsel('*','spip_auteurs','id_auteur='.($id_session+0)))
			{
				$valeurs['organisme_fiche'] = $res['nom'];
				$id_auteur= $res['auteur'];
			}
		
	
	return $valeurs;
}

function formulaires_fiche_verifier_dist($id_rubrique,$redirect=''){
	$erreurs = array();
	include_spip('inc/filtres');
	include_spip('inc/documents');
	include_spip('inc/charsets');

	
	$champs_obligatoire=array('intitule_fiche','texte_fiche');
	
	foreach($champs_obligatoire as $co)
	{
	if (!$val=_request($co))
		$erreurs[$co] = _T("info_obligatoire");
	}

	if ($nobot=_request('nobot'))
		$erreurs['nobot'] = 'Vous êtes un robot. Méchant robot.';
	
	

	
	return $erreurs;
}





function formulaires_fiche_traiter_dist($id_rubrique,$redirect=''){
	
	include_spip('base/abstract_sql');
	$id_secteur=sql_getfetsel('id_secteur','spip_rubriques','id_rubrique='.$id_rubrique);
	$tab_donnee=array(
						'titre'			=>	_request('intitule_fiche'),
						'id_rubrique'	=>	$id_rubrique,
						'texte'			=>	_request('texte_fiche'),
						'statut'		=>	'publie',
						'lang'			=>	'fr',
						'id_secteur'	=>	$id_secteur,
						'accepter_forum'=>	'oui',
						'date_modif'	=>	date('Y-m-d H:i:s'),
						'composition'	=> ''
	);
	
if(empty($id_article))
	{//print_r($tab_donnee);
		$tab_donnee['date']=$tab_donnee['date_modif'];
		$id_article=sql_insertq('spip_articles', $tab_donnee);
		include_spip('inc/session');
		$tab_donnee =	array(
			'id_auteur'=>session_get('id_auteur'),
			'id_objet'=>$id_article,
			'objet'=>'article'
			);
		sql_insertq('spip_auteurs_liens', $tab_donnee);
		
		
	}
	else
	{
		sql_updateq('spip_articles', $tab_donnee,'id_article='.($id_article+0));
		sql_delete('spip_mots_articles','id_article='.($id_article+0));
	}


	
	$message = _T("Votre idée vient d'être ajouter à la liste, merci.");
	return array('message_ok'=>$message,'editable'=>false,'redirect'=>$redirect);
}

?>