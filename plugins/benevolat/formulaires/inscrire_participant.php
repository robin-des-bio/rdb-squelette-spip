<?php

	
if (!defined("_ECRIRE_INC_VERSION")) return;
include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_inscrire_participant_charger_dist($id_benevolat_participant='new', $id_benevolat_tache=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	/* cet appel va charger dans $contexte tous les champs de la table spip_benevolat_taches associes a l'id_auteur passe en param */
	/* on a ajoute dans le contexte les metas de gestion optionnelle des champs Civilite, Prenom et Ref. Interne */

	$contexte=array();
	$contexte['_mes_saisies'] = mes_saisies_inscription($id_benevolat_tache);
	
	if(intval($id_benevolat_tache)!=0){
	$contexte['taches']=$id_benevolat_tache;
	}


	
	return $contexte;
}



function formulaires_inscrire_participant_verifier_dist($id_benevolat_participant='new', $id_benevolat_tache=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	
	$erreurs=array();
	$mes_saisies = mes_saisies_inscription($id_benevolat_tache);
	//if();
	
	$erreurs = saisies_verifier($mes_saisies);
	return array_merge($erreurs,formulaires_editer_objet_verifier('benevolat_tache', $id_auteur));
}

function formulaires_inscrire_participant_traiter_dist($id_benevolat_participant='new', $id_benevolat_tache=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	$taches=_request('taches');
	$taches=is_array($taches)?$taches:array($taches);
	foreach($taches as $id_tache)
		$result=formulaires_editer_objet_traiter('benevolat_participant',$id_benevolat_participant,$id_tache,$lier_trad,$retour,$config_fonc,$row,$hidden);
	$result['editable']="oui";
	return $result;
}



function mes_saisies_inscription($id_benevolat_tache) {

	// Champ tache
	$champ_tache=array(
	'saisie' => 'checkbox_taches',
	'options' => array(
		'nom' => 'taches',
		'label' => _T('benevolat:liste_des_taches'),
		'class' => '',
		'obligatoire' => 'oui'
		));


if(intval($id_benevolat_tache)!=0){
	$champ_tache=array(
		'saisie' => 'hidden',
		'options' => array(
			'nom' => 'taches',
		));
}

$mes_saisies = array(

	
	$champ_tache,
	
	// Champ Nom
	array(
	'saisie' => 'radio',
	'options' => array(
		'nom' => 'type',
		'label' => _T('benevolat:nom_et_prenom'),
		'class' => '',
		'obligatoire' => 'oui',
		'datas'=>array('adherent'=>'Adhérent','autre'=>'Autre')
		)),
	// Champ adherent
	array(
	'saisie' => 'auteurs',
	'options' => array(
		'nom' => 'id_auteur',
		'label' => _T('benevolat:adherent'),
		'class' => '',
		'obligatoire' => 'non'
		)),
	
	// Champ Nom
	'nom'=>array(
	'saisie' => 'input',
	'options' => array(
		'nom' => 'nom',
		'label' => _T('benevolat:nom_et_prenom'),
		'class' => '',
		'obligatoire' => 'non'
		)),
	// Champ courriel
	'courriel'=>array(
	'saisie' => 'input',
	'options' => array(
		'nom' => 'courriel',
		'label' => _T('benevolat:courriel'),
		'class' => '',
		'obligatoire' => 'non'),
	'verifier'=>array(
		'type'=>'email'
		)),
	// Champ Numero_telephone
	'telephone'=>array(
	'saisie' => 'input',
	'options' => array(
		'nom' => 'telephone',
		'label' => _T('benevolat:telephone'),
		'class' => 'masque_telephone',
		'obligatoire' => 'non',

	)));
  return $mes_saisies;
}



?>
