<?php
/***************************************************************************
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(



# Nom des tables

'nom'=>'Nom',
'details'=>'Détails',
'duree'=>'Durée',
'competence'=>'Compétence',
'nb_personne'=>'Nombre de personne souhaité',
'regularite'=>'Régularité',
'echeance'=>'Échéance',
'commentaire'=>'Commentaire',
'participants'=>'Participants',
'tache'=>'Tâche',
'taches'=>'Tâches',
'participant'=>'Participant',
'participants'=>'Participants',
	


'label_nom'=>'Nom',
'label_details'=>'Détails',
'label_duree'=>'Durée',
'label_competence'=>'Compétence',
'label_nb_personne'=>'Nombre de personne souhaité',
'label_regularite'=>'Régularité',
'label_echeance'=>'Échéance',
'label_commentaire'=>'Commentaire',
'label_fini'=>'Terminé',
'label_public'=>'Public',






# Titres globaux

	'titre_menu_gestion_association' => 'Gestion Association',


);







?>
