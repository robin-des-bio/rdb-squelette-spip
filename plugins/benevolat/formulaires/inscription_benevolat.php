<?php

	
if (!defined("_ECRIRE_INC_VERSION")) return;
include_spip('inc/actions');
include_spip('inc/editer');

function formulaires_inscription_benevolat_charger_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	/* cet appel va charger dans $contexte tous les champs de la table spip_benevolat_taches associes a l'id_auteur passe en param */
	/* on a ajoute dans le contexte les metas de gestion optionnelle des champs Civilite, Prenom et Ref. Interne */

	$contexte=array();
	$contexte['_mes_saisies'] = mes_saisies_inscription();

	$contexte['nom']=_request('nom');
	$contexte['courriel']=_request('courriel');
	
	return $contexte;
}



function formulaires_inscription_benevolat_verifier_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	
	$erreurs=array();
	$mes_saisies = mes_saisies_inscription();
	$erreurs = saisies_verifier($mes_saisies);
	
	return array_merge($erreurs,formulaires_editer_objet_verifier('benevolat_tache', $id_auteur));
}

function formulaires_inscription_benevolat_traiter_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	

	include_spip('inc/config');
	include_spip('inc/notifications');
	//$id_auteur=session_get('id_auteur');
	$tab_data = array(
	//			'id_auteur'=>$id_auteur,
				'nom'=>_request('nom'),
				'telephone'=>_request('telephone'),
				'courriel'=>_request('courriel'),
				'date_inscription'=>date('Y-m-d H:i:s')
			);
	$taches=_request('taches');
	foreach($taches as $tache){
		$tab_data['id_benevolat_tache']=$tache;
		sql_insertq('spip_benevolat_participants',$tab_data);
		}
	$emails=lire_config('benevolat/notifications');
	notifications_envoyer_mails($emails, 'Un adhérent vient de s\'inscrire sur le tableau des bénévoles', "[RdB] Inscription bénévolat");
	$result['message_ok']='Merci, votre inscription a bien été enregistré. On vous contacte bientôt. <i>L\'équipe bénévolat</i>';
	return $result;



}



function mes_saisies_inscription() {



$mes_saisies = array(



		// Champ tache
		array(
		'saisie' => 'checkbox_taches',
		'options' => array(
			'nom' => 'taches',
			'label' => _T('benevolat:liste_des_taches'),
			'class' => '',
			'obligatoire' => 'oui'
			)),


		// Champ Nom
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'nom',
			'label' => _T('benevolat:nom_et_prenom'),
			'class' => '',
			'obligatoire' => 'oui'
			)),
		// Champ courriel
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'courriel',
			'label' => _T('benevolat:courriel'),
			'class' => '',
			'obligatoire' => 'oui'),
		'verifier'=>array(
			'type'=>'email'
			)),
		// Champ Numero_telephone
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'telephone',
			'label' => _T('benevolat:telephone'),
			'class' => 'masque_telephone',
			'obligatoire' => 'oui',

			))
	
	

  );
  return $mes_saisies;
}



?>
