<?php

$GLOBALS[$GLOBALS['idx_lang']] = array(
'pass_mail_activation' => '
Pour activer votre acc&egrave;s au site
@nom_site_spip@ (@adresse_site@)

Veuillez vous rendre &agrave; l\'adresse suivante :

    @sendcookie@

Vous pourrez alors entrer un mot de passe
et vous connecter à votre espace adh&eacute;rent.

',
'pass_recevoir_mail_activation' => 'Vous allez recevoir un email vous indiquant comment poursuivre la proc&eacute;dure d\'activation.'


);

?>
