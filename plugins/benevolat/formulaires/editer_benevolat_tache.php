<?php

	
if (!defined("_ECRIRE_INC_VERSION")) return;
include_spip('inc/actions');
include_spip('inc/editer');
/***************************************************************************\
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/
function formulaires_editer_benevolat_tache_charger_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	/* cet appel va charger dans $contexte tous les champs de la table spip_benevolat_taches associes a l'id_auteur passe en param */
	/* on a ajoute dans le contexte les metas de gestion optionnelle des champs Civilite, Prenom et Ref. Interne */

	$contexte = formulaires_editer_objet_charger('benevolat_tache',$id_benevolat_tache,$id_rubrique,$lier_trad,$retour,$config_fonc,$row,$hidden);
	$contexte['mes_saisies'] = mes_saisies_tache();

	return $contexte;
}




function formulaires_editer_benevolat_tache_identifier_dist($id_auteur='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden=''){

        return serialize(array(intval($id_auteur)));

}


function formulaires_editer_benevolat_tache_verifier_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	
	$erreurs=array();
	$mes_saisies = mes_saisies_tache();
	$erreurs = saisies_verifier($mes_saisies);
	return array_merge($erreurs,formulaires_editer_objet_verifier('benevolat_tache', $id_auteur));
}

function formulaires_editer_benevolat_tache_traiter_dist($id_benevolat_tache='new', $id_rubrique=0, $retour='', $lier_trad=0, $config_fonc='', $row=array(), $hidden='') {
	

	if($id_benevolat_tache=='new')
		set_request('date_creation',date('Y-m-d H:m:s'));	
		
	return formulaires_editer_objet_traiter('benevolat_tache', $id_benevolat_tache,$id_rubrique,$lier_trad,generer_url_ecrire('benevolat_tache','id_benevolat_tache='.$id_benevolat_tache),$config_fonc,$row,$hidden);
}



function mes_saisies_tache() {



						 



$mes_saisies = array(


	

		// Champ Nom
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'nom',
			'label' => _T('benevolat_tache:nom'),
			'class' => '',
			'obligatoire' => 'oui'
			)),
		// Champ détail
		array(
		'saisie' => 'textarea',
		'options' => array(
			'nom' => 'details',
			'label' => _T('benevolat_tache:details'),
			'class' => '',
			'obligatoire' => 'oui',
			'rows'=>5
			)),
	
		// Durée
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'duree',
			'label' => _T('benevolat_tache:duree'),
			'class' => '',
			'obligatoire' => 'non'
			)),
		// Compétence
		array(
		'saisie' => 'textarea',
		'options' => array(
			'nom' => 'competence',
			'label' => _T('benevolat_tache:competence'),
			'class' => '',
			'obligatoire' => 'non',
			'rows'=>5
			)),
		// Nombre de personne
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'nb_personne',
			'label' => _T('benevolat_tache:nb_personne'),
			'class' => '',
			'obligatoire' => 'non',
			)),
		// Régularité
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'regularite',
			'label' => _T('benevolat_tache:regularite'),
			'class' => '',
			'obligatoire' => 'non'
			)),
		// échéance
		array(
		'saisie' => 'input',
		'options' => array(
			'nom' => 'echeance',
			'label' => _T('benevolat_tache:echeance'),
			'class' => '',
			'obligatoire' => 'non'
			)),
		// Commentaire
		array(
		'saisie' => 'textarea',
		'options' => array(
			'nom' => 'commentaire',
			'label' => _T('benevolat_tache:commentaire'),
			'class' => '',
			'obligatoire' => 'non',
			'rows'=>5
			)),
		// fini
		array(
		'saisie' => 'oui_non',
		'options' => array(
			'nom' => 'fini',
			'label' => _T('benevolat_tache:fini'),
			'class' => '',
			'obligatoire' => 'oui',
			'valeur_oui'=> '1',
			'valeur_non'=> '0'
			)),	
			
			
		// Public
		array(
		'saisie' => 'oui_non',
		'options' => array(
			'nom' => 'public',
			'label' => _T('benevolat_tache:public'),
			'class' => '',
			'obligatoire' => 'oui',
			'valeur_oui'=> 1,
			'valeur_non'=> 0
			))
  );
  return $mes_saisies;
}



?>
