<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;




function action_editer_info_adherent_dist($arg=null) {

	if (is_null($arg)){
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}
	
	include_spip('action/editer_asso_membre');
	include_spip('action/editer_auteur');
	include_spip('action/editer_numero');
	include_spip('action/supprimer_numero');
	include_spip('action/editer_adresse');
	// si id_auteur n'est pas un nombre, c'est une creation
	$nouveau=false;
        
    if (!$id_auteur = intval($arg))
		return array(0,'erreur'=>'Problème');
	
	if ($id_auteur!=session_get('id_auteur'))
		return array(0,'erreur'=>'Problème');

	$tab_data = array('email'=>_request('email'));
	$tab_data['telephone']=_request('telephone');
	$tab_data['mobile']=_request('mobile');
	$tab_data['fax']=_request('fax');
	$tab_data['adresse']=_request('adresse');
	$tab_data['code_postal']=_request('code_postal');
	$tab_data['ville']=_request('ville');
	$tab_data['pays']=_request('pays');
	// Enregistre l'envoi dans la BD
	$err = "";
	$err = auteur_modifier($id_auteur,$tab_data);
	if ($err)
		spip_log("echec editeur auteur: $err",_LOG_ERREUR);
	return $err;
}


?>
