<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


function benevolat_participant_informations($id_auteur,$nom="",$courriel="",$telephone="",$mobile=""){
	$tab_res=array();
	if(intval($id_auteur)!=0){
		$membre = sql_allfetsel('a.id_auteur as id_auteur,concat(prenom,\' \',nom_famille) as nom,a.nom as nom2,email,telephone,mobile','spip_auteurs a','a.id_auteur='.intval($id_auteur));
		if($membre){
			$tab_res=array('id_auteur'=>$id_auteur);
			$nom_temp = empty($membre[0]['nom'])?$membre[0]['nom2']:$membre[0]['nom'];
			$nom = empty($nom)?$nom_temp:$nom;
			$courriel = empty($courriel)?$membre[0]['email']:$courriel;
			$telephone = empty($telephone)?$membre[0]['telephone']:$telephone;
			$telephone = empty($telephone)?$mobile:$telephone;
			}
		}
	return array_merge($tab_res,array('nom'=>$nom,'courriel'=>$courriel,'telephone'=>$telephone));
}



function benevolat_descript_participant($participants){
$tab_participant=array();
$tab_courriels=unserialize($participants);
foreach($tab_courriels as $key => $courriel){
	
	if(intval($courriel)!=0){
		$membre = sql_allfetsel('a.id_auteur as id_auteur,concat(prenom,\' \',nom_famille) as nom,a.nom as nom2,email','spip_auteurs a  left outer join spip_asso_membres m on a.id_auteur=m.id_auteur ','a.id_auteur='.intval($courriel));
		if(isset($membre[0]['email'])){
			$membre[0]['nom']=empty($membre[0]['nom'])?$membre[0]['nom2']:$membre[0]['nom'];
			$membre[0]['nom']=isset($membre[0]['nom'])?$membre[0]['nom2']:$membre[0]['nom'];
			unset($membre[0]['nom2']);
			$tab_participant[]=$membre[0];
			}
		}
	elseif(is_array($courriel)){
		$membre = sql_allfetsel('a.id_auteur as id_auteur,concat(prenom,\' \',nom_famille) as nom,a.nom as nom2,email','spip_auteurs a  left outer join spip_asso_membres m on a.id_auteur=m.id_auteur ','a.id_auteur='.intval($courriel));
		if(isset($membre[0]['email'])){
			$membre[0]['nom']=empty($membre[0]['nom'])?$membre[0]['nom2']:$membre[0]['nom'];
			$membre[0]['nom']=isset($membre[0]['nom'])?$membre[0]['nom2']:$membre[0]['nom'];
			unset($membre[0]['nom2']);
			$tab_participant[]=$membre[0];
			}
		}
	elseif(strpos($courriel,'|')!== false)
		{
		$c=explode('|',$courriel);	
		
		if(strpos($c[0],'@')!== false)	
			$tab_participant[]=array('nom'=>$c[1],'email'=>$c[0]);
		else
			$tab_participant[]=array('nom'=>$c[0],'email'=>$c[1]);
		}
	elseif(strpos($courriel,'@')!== false)
		$tab_participant[]=array('nom'=>'','email'=>$courriel);
	}
	return $tab_participant;
}




function benevolat_liste_email($id_benevolat_tache){
	
	$tab_participant=sql_allfetsel('*','spip_benevolat_participants','id_benevolat_tache='.intval($id_benevolat_tache));
	$tab_courriels=array();
	foreach($tab_participant as &$p){
		$p=benevolat_participant_informations($p['id_auteur'],$p['nom'],$p['courriel'],$p['telephone']);
		if($p['courriel'])
			$tab_courriels[]=trim($p['nom']." <".$p['courriel'].">");
	}
	return implode(';',$tab_courriels);	
}


function benevolat_liste_participant($participants,$avec_courriel=false,$avec_lien=true){
	$tab_courriels=benevolat_participant_informations($participants);
	
	
	foreach($tab_courriels as &$courriel){
		$tmp_courriel= ($avec_courriel ? trim($courriel['nom']." <".$courriel['email'].">"):(empty($courriel['nom'])?$courriel['email']:$courriel['nom']));
		$courriel=$avec_lien && isset($courriel['id_auteur']) ?'<a href="'.generer_url_entite($courriel['id_auteur'],'asso_membre').'">'.$tmp_courriel.'</a>':$tmp_courriel;
	}
	if(empty($tab_courriels))
		{return _T('benevolat_tache:aucun_participant_pour_le_moment');}
return '<ul><li>'.implode('</li><li>',$tab_courriels).'</li></ul>';	
}

