<?php

if (!defined("_ECRIRE_INC_VERSION")) return;

function action_supprimer_benevolat_tache_dist(){
	$securiser_action = charger_fonction('securiser_action','inc');
	$arg = $securiser_action();
	$id_benevolat_tache = intval($arg);
	sql_delete('spip_benevolat_taches', 'id_benevolat_tache='.$id_benevolat_tache);
	redirige_url_ecrire('benevolat_taches');
}
?>
